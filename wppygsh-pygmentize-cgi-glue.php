<?php
/*
 * Copyright (c) 2015, hhsprings <https://bitbucket.org/hhsprings/>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * 
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in
 *   the documentation and/or other materials provided with the
 *   distribution.
 * 
 * - Neither the name of the hhsprings nor the names of its contributors
 *   may be used to endorse or promote products derived from this software
 *   without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

// ----------------------------------------------------------
if ( !defined( 'ABSPATH' ) ) return;

require_once( 'wppygsh-config.php' );
require_once( 'wppygsh-utils.php' );
require_once( 'wppygsh-logging.php' );
require_once( 'wppygsh-options.php' );

// ----------------------------------------------------------
/*
 * call pygmentize.cgi as CGI.
 */
function call_pygmentize_cgi_webservice( $params, $outtype, &$error ) {

	$curl = curl_init();

	// User Agent must be set against for WAF (Web Application
    // Firewall) or some .htaccess settings.
	$ua = 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)';
	$verbose_stream = tmpfile(); // receiver stream for cURL verbose output
	curl_setopt_array( $curl, array(
		CURLOPT_HEADER         => FALSE,
		CURLOPT_RETURNTRANSFER => TRUE,
		CURLOPT_VERBOSE        => TRUE,
		CURLOPT_STDERR         => $verbose_stream,
		CURLOPT_FAILONERROR    => TRUE, // fail on error code >= 400
		CURLOPT_USERAGENT => $ua,
		CURLOPT_REFERER => home_url( '/' ),
		CURLOPT_ENCODING => "gzip,deflate",
	) );
	$header = array(
		"Accept-Charset: utf-8",
		"Content-Type: application/json",
	);
	if ( $outtype == "json" ) {
		$header[] = "Accept: application/json";
	} else {
		$header[] = "Accept: text/html";
	}
	curl_setopt( $curl, CURLOPT_HTTPHEADER, $header );

	curl_setopt( $curl, CURLOPT_URL, wppygsh_get_service_cgi() );
	curl_setopt( $curl, CURLOPT_RETURNTRANSFER, 1 );
	curl_setopt( $curl, CURLOPT_POST, 1 );

	$post_data = json_encode( $params );

	curl_setopt( $curl, CURLOPT_POSTFIELDS, $post_data );
	$output = curl_exec( $curl );

	if ( $output === FALSE ) {
		echo curl_error( $curl ) . PHP_EOL;

		// dump verbose output
		fseek( $verbose_stream, 0 );
		while ( ( $line = fgets( $verbose_stream ) ) !== FALSE ) {
			$tmp .= $line;
		}
		echo '<pre style="font-size: xx-small; line-height: 1.1;">' . $tmp . '</pre>';
		$error = TRUE;
	}
	curl_close( $curl );

	return $output;
}

/*
 * call pygmentize.cgi directly.
 */
function call_pygmentize_cgi_direct( $params, $outtype, &$error ) {
	$error = FALSE;

	$descriptorspec = array(
		0 => array( "pipe", "r" ),  // stdin for child process
		1 => array( "pipe", "w" ),  // stdout for child process
		2 => array( "pipe", "w" ),  // stderr for child process
	);
	$wd = "";
	$cmd = wppygsh_get_service_shellcmd( $wd );

	$myenv = array();
	$myenv['REQUEST_METHOD'] = 'POST';
	$myenv['CONTENT_TYPE'] = 'application/json';
	$myenv['HTTP_ACCEPT_CHARSET'] = 'utf-8';
	if ( $outtype == "json" ) {
		$myenv['HTTP_ACCEPT'] = "application/json";
	} else {
		$myenv['HTTP_ACCEPT'] = "text/html";
	}
	// In direct mode, no need compress with gzip.
	unset( $myenv['HTTP_ACCEPT_ENCODING'] );

	$process = proc_open(
		$cmd,
		$descriptorspec, $pipes, $wd, $myenv );

	$result = array();
	if ( is_resource( $process ) ) {
		$post_data = json_encode( $params );

		fwrite( $pipes[0], $post_data );
		fclose( $pipes[0] );

		$cntstdout = stream_get_contents( $pipes[1] );
		$cntstderr = stream_get_contents( $pipes[2] );
		fclose( $pipes[1] );
		fclose( $pipes[2] );
		$header = TRUE;
		foreach ( explode( "\n", $cntstdout ) as $line ) {
			if ( $header ) {
				if ( preg_match( '/^$/', $line ) ) {
					$header = FALSE;
				}
			} else {
				$result[] = $line;
			}
		}

		$return_value = proc_close( $process );
		if ( $return_value != 0 ) {
			echo $cntstderr;
			$error = TRUE;
		}
	}
	return implode( PHP_EOL, $result );
}
// ----------------------------------------------------------

/*
 * call pygmentize.cgi with proper scheme described in
 * wordpress option table.
 */
function call_pygmentize_cgi( $service_params, $outtype, &$error ) {
	if ( WPPYGSH_Opts::instance()->call_pygmentize_scheme != 'cgi' ) {
		return call_pygmentize_cgi_direct(
			$service_params, $outtype, $error );
	} else {
		return call_pygmentize_cgi_webservice(
			$service_params, $outtype, $error );
	}
}
