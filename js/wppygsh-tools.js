/*
 * Copyright (c) 2015, hhsprings <https://bitbucket.org/hhsprings/>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * 
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in
 *   the documentation and/or other materials provided with the
 *   distribution.
 * 
 * - Neither the name of the hhsprings nor the names of its contributors
 *   may be used to endorse or promote products derived from this software
 *   without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
jQuery(function($) {

  function toggle_ln(linenos, show) {
    linenos.each(function(index) {
      var e = $(this);
      var div = e.parent();
      while (div.prop("tagName").toLowerCase() != "div") {
        div = div.parent();
      }
      var borderColor = "rgb(170, 170, 170)";
      var borderColorDiv = div.css('border-color');
      if (borderColorDiv) {
        borderColor = borderColorDiv;
      }
      var lnstyles = {
        'border': '1px solid ' + borderColor,
        'background-color': div.parent().parent().css('background-color'),
        'padding-left': '1em',
        'margin-right': '1em'
      };

      if (show) {
        e.css(lnstyles);
        e.css('display', 'inline');
      } else {
        e.css(lnstyles);
        e.css('display', 'none');
      }
    });
  }
  function selectText(el) {
    var doc = document;
    if (doc.body.createTextRange) {
      var range = doc.body.createTextRange();
      range.moveToElementText(el);
      range.select();
    } else if (window.getSelection) {
      var range = doc.createRange();
      range.selectNodeContents(el);
      var selection = window.getSelection();
      selection.removeAllRanges();
      selection.addRange(range);
    }
  }

  $(document).ready(function() {
    /* Add a [>>>] button on the top-right corner of code samples to hide
     * the >>> and ... prompts and the output and thus make the code
     * copyable.
     * Add a [+#] button on the top-left corner of code samples to toggle
     * linenos. */

    var div = $('.pygments .highlight');
    var pre = div.find('pre');

    pre.each(function (i) {
      /* double-click to select all, mainly for chrome...*/
      $(this).on('dblclick', function(e) {
        if (e.ctrlKey) {
          var head = $('head').html();
          var contents =
            '<div class="pygments"><div class="highlight"><pre title="Ctrl-A to select all">' +
            $(this).html() +
            '</pre></div></div>';
          var nw = window.open();
          nw.document.head.innerHTML = head;
          $(nw.document.body).html(contents);
        } else {
          selectText(this);
        }
      });
      this.title = 'double-click to select all.\n\nctrl-double-click to new window.';
    });

    var ln_first_visible = false;

    toggle_ln(div.find(".lineno"), ln_first_visible);

    // get the styles from the current theme
    pre.parent().parent().css('position', 'relative');
    var border_width = div.css('border-top-width');
    var border_style = div.css('border-top-style');
    var border_color = div.css('border-top-color');
    var button_styles = {
      'cursor':'pointer', 'position': 'absolute', 'top': '0', 'right': '0',
      'border-color': border_color, 'border-style': border_style,
      'border-width': border_width, 'color': border_color, 'text-size': '75%',
      'font-family': 'monospace', 'padding-left': '0.2em', 'padding-right': '0.2em'
    }
    var lnbutton_styles = {
      'cursor':'pointer',
      'position': 'absolute', 'top': '0em', 'left': '0',
      'border-color': border_color, 'border-style': border_style,
      'border-width': border_width, 'color': border_color,
      'text-size': '75%',
      'font-family': 'monospace',
      'padding-left': '0.2em', 'padding-right': '0.2em'
    }
    var hide_text = 'Hide the prompts and ouput';
    var show_text = 'Show the prompts and ouput';
    var lnhide_text = 'Hide the linenos';
    var lnshow_text = 'Show the linenos';
  
    // create and add the button to all the code blocks that contain >>>
    // create and add the button to all the code blocks that contain lineno
    div.each(function(index) {
      var jthis = $(this);
      if (jthis.find('.lineno').length > 0) {
        var button = $('<span class="linenosbutton">+#</span>');
        button.css(lnbutton_styles);
        button.attr('title',
                    ln_first_visible ? lnhide_text : lnshow_text);
        jthis.prepend(button);
      }
      if (jthis.find('.gp').length > 0) {
        var button = $('<span class="copybutton">&gt;&gt;&gt;</span>');
        button.css(button_styles);
        button.attr('title', hide_text);
        jthis.prepend(button);
      }
      // tracebacks (.gt) contain bare text elements that need to be
      // wrapped in a span to work with .nextUntil() (see later)
      jthis.find('pre:has(.gt)').contents().filter(function() {
        return ((this.nodeType == 3) && (this.data.trim().length > 0));
      }).wrap('<span>');
    });
  
    // define the behavior of the button when it's clicked
    $('.copybutton').toggle(
      function() {
        var button = $(this);
        button.parent().find('.go, .gp, .gt').hide();
        button.parent().find('.gt').nextUntil('.gp, .go').css('visibility', 'hidden');
        button.css('text-decoration', 'line-through');
        button.attr('title', show_text);
      },
      function() {
        var button = $(this);
        button.parent().find('.go, .gp, .gt').show();
        button.parent().find('.gt').nextUntil('.gp, .go').css('visibility', 'visible');
        button.css('text-decoration', 'none');
        button.attr('title', hide_text);
      });
    $('.linenosbutton').on('click',
      function() {
        var button = $(this);
        var hidden = button.attr('title') == lnhide_text;
        toggle_ln(
          button.parent().find(".lineno"), !hidden);
        button.attr('title', hidden ? lnshow_text : lnhide_text);
      });
  });
});
