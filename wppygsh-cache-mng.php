<?php
/*
 * Copyright (c) 2015, hhsprings <https://bitbucket.org/hhsprings/>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * 
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in
 *   the documentation and/or other materials provided with the
 *   distribution.
 * 
 * - Neither the name of the hhsprings nor the names of its contributors
 *   may be used to endorse or promote products derived from this software
 *   without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
// ----------------------------------------------------------
if ( !defined('ABSPATH') ) return;

require_once( 'wppygsh-config.php' );
require_once( 'wppygsh-utils.php' );
require_once( 'wppygsh-logging.php' );
require_once( 'wppygsh-options.php' );

// ----------------------------------------------------------
global $wppygsh_db_version;
$wppygsh_db_version = '1.0';

function wppygsh_install() {
	chmod( WPPYGSH_PLUGIN_DIR . '/' . 'cgi-bin/pygmentize.cgi', 0700 );
	chmod( WPPYGSH_PLUGIN_DIR . '/' . 'cgi-bin/pygmentize.cgi.cfg', 0600 );
	chmod( WPPYGSH_PLUGIN_DIR . '/' . 'cgi-bin/pygmentize_cgi.py', 0600 );

	global $wpdb;
	global $wppygsh_db_version;

	$table_name = $wpdb->prefix . 'wppygsh_cache';

	$charset_collate = $wpdb->get_charset_collate();

	$sql = "CREATE TABLE $table_name (
  post_id bigint(20) unsigned NOT NULL,
  code_hash varchar(32) NOT NULL,
  params_hash varchar(32) NOT NULL,
  cache_value mediumtext NOT NULL,
  cache_value_compressed tinyint(1) NOT NULL,
  UNIQUE KEY thekey (post_id, code_hash)
  ) $charset_collate;";

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );

	add_option( 'wppygsh_db_version', $wppygsh_db_version );
}

register_activation_hook( __FILE__, 'wppygsh_install' );

// ----------------------------------------------------------
/*
 *
 */
global $wpdb;
if ( !isset( $wpdb->wppygsh_cache ) ) {
	$wpdb->wppygsh_cache = $wpdb->prefix . "wppygsh_cache";
}

// ----------------------------------------------------------
/*
 *
 */
final class All_Codehash_In_Post {
	private $code_hashes = array();

	private static $inst;

	/*
	 *
	 */
	public static function instance() {
		if ( static::$inst === null ) {
			static::$inst = new static();
		}
		return static::$inst;
	}

	/*
	 *
	 */
	private function __construct() {
	}

	/*
	 *
	 */
	public function append( $code_hash ) {
		$this->code_hashes[] = $code_hash;
	}

	/*
	 *
	 */
	public function cleanup_unused_cache() {
		global $wpdb;
		global $post;

		$usedsa = array();
		foreach ( $this->code_hashes as $used ) {
			$usedsa[] = "'" . $used . "'";
		}
		$wh = "";
		if ( !empty( $usedsa ) ) {
			$wh = " AND code_hash NOT IN (" . implode( ", ", $usedsa ) . ")";
		}
		$wpdb->query(
			"DELETE " .
			"FROM $wpdb->wppygsh_cache " .
			"WHERE post_id = " . $post->ID . $wh );
	}
}

/*
*
*/
function wppygsh_cleanup_unused_cache() {
	WPPYGSH_Perf_Log::start( "wppygsh_cleanup_unused_cache" );
	if ( $_GET['preview'] == "true" ) {
		WPPYGSH_Perf_Log::chkpt( "previewing" );
		All_Codehash_In_Post::instance()->cleanup_unused_cache();
	}
	WPPYGSH_Perf_Log::stop( "" );
}
add_action( 'wp_footer', 'wppygsh_cleanup_unused_cache' );

/*
*
*/
class Pygmentized_Cache {
	private $updkey = array();
	private $from_dbcache = array();
	private $params_hash = "";

	/*
	 *
	 */
	public function __construct( $service_params ) {
		global $wpdb;
		global $post;

		$code_hash = hash( 'md4', $service_params["params"]["code"] );
		All_Codehash_In_Post::instance()->append( $code_hash );
		$hash_targets = array();
		foreach ( $service_params['params'] as $k => $v ) {
			if ( $k == 'code' ) {
				continue;
			}
			$hash_targets[] = "" . $v;
		}
		$hash_targets[] = WPPYGSH_VERSION;
		$this->params_hash = hash( 'md4', implode( "-", $hash_targets ) );

		$query = $wpdb->prepare(
			"SELECT" . 
			"  post_id, " .
			"  code_hash, " .
			"  params_hash, " .
			"  cache_value, " .
			"  cache_value_compressed " .
			"FROM $wpdb->wppygsh_cache " .
			"WHERE post_id = %d AND code_hash = '%s'",
			$post->ID, $code_hash );
		$this->from_dbcache = $wpdb->get_row( $query, ARRAY_A, 0 );
		$this->updkey = array(
			'post_id' => $post->ID,
			'code_hash' => $code_hash );

		if ( empty( $this->from_dbcache ) ) {
			WPPYGSH_Perf_Log::chkpt( "cache NF" );
		} elseif ( $this->from_dbcache['params_hash'] != $params_hash ) {
			WPPYGSH_Perf_Log::chkpt( "cache OB" );
		}
	}

	/*
	 *
	 */
	private function require_refresh() {
		return
			empty( $this->from_dbcache ) or
			$this->from_dbcache['params_hash'] != $this->params_hash;
	}

	/*
	 *
	 */
	public function get_cache() {
		if ( $this->require_refresh() ) {
			return "";
		}
		$cache_value = $this->from_dbcache['cache_value'];
		if ( $this->from_dbcache['cache_value_compressed'] ) {
			$cache = gzuncompress( base64_decode( $cache_value ) );
			WPPYGSH_Perf_Log::chkpt( "compressed cache found" );
		} else {
			$cache = base64_decode( $cache_value );
			WPPYGSH_Perf_Log::chkpt( "cache found" );
		}
		return $cache;
	}

	/*
	 *
	 */
	public function update_cache( $output ) {
		global $wpdb;

		if ( strlen( $output ) < WPPYGSH_CACHE_COMPRESS_THR ) {
			$cache_value = base64_encode( $output );
			$cache_value_compressed = 0;
			WPPYGSH_Perf_Log::chkpt( "compression is not required" );
		} else {
			$cache_value = base64_encode(
                gzcompress(
                    $output,
                    WPPYGSH_CACHE_COMPRESS_LVL ) );
			$cache_value_compressed = 1;
			WPPYGSH_Perf_Log::chkpt( "compression is required" );
		}
		$updrow = array(
			'post_id' => $this->updkey['post_id'],
			'code_hash' => $this->updkey['code_hash'],
			'params_hash' => $this->params_hash,
			'cache_value' => $cache_value,
			'cache_value_compressed' => $cache_value_compressed,
		);
		if ( empty( $this->from_dbcache ) ) {
			$affcnt = $wpdb->insert(
				$wpdb->wppygsh_cache, $updrow );
		} else {
			$affcnt = $wpdb->update(
				$wpdb->wppygsh_cache, $updrow, $this->updkey );
		}
	}
}
