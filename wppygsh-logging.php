<?php
/*
 * Copyright (c) 2015, hhsprings <https://bitbucket.org/hhsprings/>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * 
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in
 *   the documentation and/or other materials provided with the
 *   distribution.
 * 
 * - Neither the name of the hhsprings nor the names of its contributors
 *   may be used to endorse or promote products derived from this software
 *   without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

// ----------------------------------------------------------
if ( !defined( 'ABSPATH' ) ) return;
require_once( 'wppygsh-config.php' );

class WPPYGSH_Perf_Log {
	private static $file = NULL;

	private static $start_time = 0;
	private static $title = "";

	function __construct() {}

	public static function start( $title ) {
		if ( WPPYGSG_PERFLOG_ENABLED ) {
			self::$title = $title;
			self::$start_time = microtime( TRUE );
		}
	}
	public static function chkpt( $title ) {
		if ( WPPYGSG_PERFLOG_ENABLED ) {
			self::$title .= " " . $title;
		}
	}

	public static function stop( $title ) {
		if ( WPPYGSG_PERFLOG_ENABLED ) {
			$end_time = microtime( TRUE );
			if ( self::$file == NULL ) {
				self::$file = @fopen( WPPYGSG_PERFLOG, 'a+' );
			}
			if ( filesize( WPPYGSG_PERFLOG ) > WPPYGSG_LOG_MAX_SIZE ) {
				ftruncate( self::$file, 0 );
			}
			$ms = ( $end_time - self::$start_time ) * 1000;
			$logrecord = '[' . date( 'Y-m-d H:i:s' ) . '] ' .
					   self::$title . ' -> ' . $title . ' {' . $ms .
					   ' [msec]}' . PHP_EOL;
			fwrite( self::$file, $logrecord );
			self::$start_time = $end_time;
		}
	}
}
