<?php
/*
 * Copyright (c) 2015, hhsprings <https://bitbucket.org/hhsprings/>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * 
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in
 *   the documentation and/or other materials provided with the
 *   distribution.
 * 
 * - Neither the name of the hhsprings nor the names of its contributors
 *   may be used to endorse or promote products derived from this software
 *   without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

// ----------------------------------------------------------
if ( !defined( 'ABSPATH' ) ) return;

require_once( 'wppygsh-config.php' );
require_once( 'wppygsh-utils.php' );
require_once( 'wppygsh-logging.php' );
require_once( 'wppygsh-options.php' );
require_once( 'wppygsh-pygmentize-cgi-glue.php' );

// ----------------------------------------------------------
/*
 * get lexers
 */
function get_pygments_lexers() {
	$service_params = array(
		"command" => "lexers",
		"params" => array(
			"fields" => array( "name", "aliases" )
		)
	);
	$output = call_pygmentize_cgi( $service_params, "json", $error );
	$output = json_decode( $output );
	if ( !$error ) {
		$result = "";
		foreach ( $output as $row ) {
			$fullname = $row[0];
			$name = $row[1][0];
			$result .= '<option value="';
			$result .= $name;
			$result .= '"';
			if ( $name == WPPYGSH_Opts::instance()->default_lang ) {
				$result .= ' selected';
			}
			$result .= ">";
			$result .= $fullname;
			$result .= "</option>";
		}
		return $result;
	}
	return "";
}

/*
 * enumerate styles with listing css files in css/.
 */
function wppygsh_enum_styles() {
	$cssall = glob( WPPYGSH_PLUGIN_DIR . "/css/theme/*.css" );
	$result = array();
	foreach ( $cssall as $f ) {
		$b = basename( $f, ".css" );
		if ( $b != "wppygsh_base" ) {
			$result[] = $b;
		}
	}
	return $result;
}

/*
 * build href
 */
function wppygsh_build_href( $url, $query, $text, $title="" ) {
    if ( empty( $title ) ) {
        $title = $url;
    }
    if ( !empty( $query ) ) {
        $query = "?" . $query;
    }
    return "<a href='$url$query' title='$title' target='_blank'>$text</a>";
}
