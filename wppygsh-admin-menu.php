<?php
/*
 * Copyright (c) 2015, hhsprings <https://bitbucket.org/hhsprings/>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * 
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in
 *   the documentation and/or other materials provided with the
 *   distribution.
 * 
 * - Neither the name of the hhsprings nor the names of its contributors
 *   may be used to endorse or promote products derived from this software
 *   without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
// ----------------------------------------------------------
if ( !defined( 'ABSPATH' ) ) return;

require_once( 'wppygsh-config.php' );
require_once( 'wppygsh-utils.php' );
require_once( 'wppygsh-logging.php' );
require_once( 'wppygsh-options.php' );
require_once( 'wppygsh-pygmentize-cgi-glue.php' );
require_once( 'wppygsh-ui-common.php' );
// ----------------------------------------------------------

add_action( 'admin_menu', 'wppygsh_plugin_menu' );

function wppygsh_plugin_menu() {
	add_options_page(
        'WP Pygments Syntax Highlighter',
        'WP Pygments Syntax Highlighter',
        'manage_options',
        'wp_pygments_syntax_highlighter',
        'wppygsh_plugin_options' );
}

/*later class{*/
function head() {
	$myurldir = wppygsh_plugins_url( '.' );
	$mythemedir = wppygsh_plugins_url( 'css/theme/' );
?>
  <script type="text/javascript">
  // <![CDATA[
  jQuery(function($) {
    function activate_tab(showtab) {
      var targ = [
        "service_tab", "theme_tab", "defaults_tab", "advanced_tab"
      ];
      for (var i = 0; i < targ.length; ++i) {
        if (targ[i] == showtab) {
          $('#' + targ[i]).show();
          $('#' + targ[i] + '_href').css('text-decoration', 'overline');
        } else {
          $('#' + targ[i]).hide();
          $('#' + targ[i] + '_href').css('text-decoration', 'underline');
        }
      }
    }
    $(document).ready(function() {
      activate_tab("service_tab");
      $('#service_tab_href').on(
        'click', function () { activate_tab("service_tab"); });
      $('#theme_tab_href').on(
        'click', function () { activate_tab("theme_tab"); });
      $('#defaults_tab_href').on(
        'click', function () { activate_tab("defaults_tab"); });
      $('#advanced_tab_href').on(
        'click', function () { activate_tab("advanced_tab"); });

      $('#theme').on('change', function() {
          $('#theme_preview_iframe').attr('src',
            '<?php echo $myurldir; ?>/wppygsh-theme-preview.php?theme=' +
            "<?php echo $mythemedir; ?>" +
            $('#theme').val() + ".css");
        });

    });
  });
  // ]]>
  </script>

<div class="wrap">
<h2>WP Pygments Syntax Highlighter <?php _e( 'Settings' ); ?></h2>
<form id="wppygsh_form" name="wppygsh_form" action="" method="POST">
<a id="service_tab_href" style="cursor: pointer">
External Service Settings</a>|
<a id="theme_tab_href" style="cursor: pointer">
Theme</a>|
<a id="defaults_tab_href" style="cursor: pointer">
Parameter Defaults</a>|
<a id="advanced_tab_href" style="cursor: pointer">
Advanced</a>
      <p class="submit">
        <input type="submit" name="Submit" class="button-primary"
         value="<?php esc_attr_e( 'Save Changes' ); ?>" />
      </p>
<?php
	wp_nonce_field( 'wp_pygments_syntax_highlighter', '_wppygsh___nonce' );
}
function foot() {
?>
      <p class="submit">
        <input type="submit" name="Submit" class="button-primary"
         value="<?php esc_attr_e( 'Save Changes' ); ?>" />
      </p>
      </p>
    </form>
  </div>
<?php
}
function table_row( $head, $comment_array, $input ) {
	$comment = implode( "", $comment_array );
	echo <<<EOT
      <tr valign="top">
        <th scope="row" style="width: 80px;">
          $head
        </th>
        <td style="horizontal-align: left">
          $input
          <p class="description">$comment</p>
        </td>
      </tr>
EOT;
}
/*}later class*/

function wppygsh_plugin_options() {
	if ( !current_user_can( 'manage_options' ) ){
		wp_die(
			__( 'You do not have sufficient permissions to access this page.' ) );
	}
	$theme_field = 'theme';
	$default_lang_field = 'default_lang';

	$theme_options = wppygsh_enum_styles();

	if ( $_POST ) {
		$orig_scs = wppygsh_get_altershortcodes();
		WPPYGSH_Opts::instance()->theme
			= $_POST[$theme_field];
		WPPYGSH_Opts::instance()->default_lang
			= $_POST[$default_lang_field];
		WPPYGSH_Opts::instance()->alter_shortcodes
			= preg_split(
				'/\s+/', trim( $_POST['alter_shortcodes'] ) );
		WPPYGSH_Opts::instance()->call_pygmentize_scheme
			= $_POST['call-pygmentize-scheme'];
		WPPYGSH_Opts::instance()->direct_python_interpreter_path
			= $_POST['direct_python_interpreter_path'];
		WPPYGSH_Opts::instance()->direct_extra_pythonpath
			= $_POST['direct_extra_pythonpath'];
		WPPYGSH_Opts::instance()->cgi_url
			= $_POST['cgi_url'];

		WPPYGSH_Opts::instance()->update();
		wppygsh_maint_shortcodes( $orig_scs );

    ?>
    <div class="updated">
    <p><strong><?php _e( 'Settings saved.' ); ?></strong></p>
    </div>
<?php } /*endif*/ ?>

<!-- settings area -->
<?php
	// ############################
	$pythonpath_href
		= wppygsh_build_href(
			'https://docs.python.org/2.7/using/cmdline.html',
			'highlight=pythonpath#envvar-PYTHONPATH',
			'PYTHONPATH' );
	$pygments_official_href
		= wppygsh_build_href(
			'http://pygments.org/',
			'',
			'Pygments Official Site' );
	$pygments_srctree_href
		= wppygsh_build_href(
			'https://bitbucket.org/birkenfeld/pygments-main/src',
			'',
			'Pygments source tree' );
	$pygments_lexers_href
		= wppygsh_build_href(
			'http://pygments.org/docs/lexers/',
			'',
			'Pygments Lexers' );
	head();
	// ###################
	echo '<div id="service_tab">';
	echo <<<EOT
  <table>
  <tr>
  <td style="vertical-align: top;">
EOT;
	echo '<table class="form-table">';
	// ###########
	if ( WPPYGSH_Opts::instance()->call_pygmentize_scheme == 'direct' ) {
		$dsc = "checked";
		$cgc = "";
	} else {
		$dsc = "";
		$cgc = "checked";
	}
	$direct_python_interpreter_path
		= WPPYGSH_Opts::instance()->direct_python_interpreter_path;
	$direct_extra_pythonpath
		= WPPYGSH_Opts::instance()->direct_extra_pythonpath;
	$cgi_url = WPPYGSH_Opts::instance()->cgi_url;
	$input =<<<EOT
  <table>

  <tr>
    <td>
    <input type='radio' name='call-pygmentize-scheme'
           id='call-pygmentize-scheme-direct' value='direct' $dsc />
    <label for="call-pygmentize-scheme-direct"
           style="text-decoration: underline">Direct</label>
    </td>

    <td>
    <label for="direct_python_interpreter_path" style='width: 30px'>
        Python interpreter path (ex. /usr/bin/python)</label>
    <input type='text'
      name='direct_python_interpreter_path'
      value='$direct_python_interpreter_path'
      style='width: 100%'>
    <br/>
    <label for="direct_extra_pythonpath" style='width: 30px'>
        Extra $pythonpath_href (one per line)</label>
    <br/>
    <textarea name='direct_extra_pythonpath'
        style='width: 100%; height: 100px'>$direct_extra_pythonpath</textarea>
    </td>

  </tr>

  <tr>
    <td colspan="2">
    <input type='radio' name='call-pygmentize-scheme'
           id='call-pygmentize-scheme-cgi' value='cgi' $cgc />
    <label for="call-pygmentize-scheme-cgi"
           style="text-decoration: underline">CGI</label>
    <br/>

    <label for="cgi_url" style='width: 30px'>url</label>
    <input type='text'
      name='cgi_url' value='$cgi_url'
      style='width: 100%'>
    </td>
  </tr>
  </table>
EOT;
	table_row(
		"Invocation scheme for pygmentize service settings",
		array(
			"<p>WP Pygments Syntax Highlighter has internal ",
			"pygmentize.cgi that can be called as cgi, but so ",
			"it is in the same site as plugin itself, so we can ",
			"call directly as shell command.</p><br/><p>In order ",
			"to share this cgi between sites, you can use it as ",
			"normal CGI (i.e. via HTTP) and can use external ",
			"pygmentize.cgi rather than internal. </p><br/>",
			"<p>Both path for direct and url for cgi can be ",
			"relative form, if so, this plugin will assume that ",
			"it is in this plugin's directory (like ",
			".../wp-content/plugins/wp-pygments-syntax-highlighter).</p>" ),
		$input );
	// ###########
	echo '</table>';
	echo <<<EOT
  </td>
  <td width="320px" style="vertical-align: top;">
  <!-- additional informations area -->
  <h3>Tips</h3>
  <ol>
  <li>Normally, $pythonpath_href for `Direct' is unnecessary. But if you
have no access rights to `your server' system-widely, and you can upload
Pygments package (downloaded maybe from $pygments_official_href) to
WordPress contents dir (actually you can!), $pythonpath_href is useful.</li>
  <li>If you have downloaded $pygments_srctree_href, and you want to
upload to your server, you don't need to upload whole tree but just need
to upload `pygments' subdir.</li>
  <li>Interpreter and $pythonpath_href setting have no effect on cgi mode
if url is external site. In other words, <strong>those have effect if url
is site-local even if CGI mode</strong>.</li>
  <li>If your server has Python 3.x, and you want to use it, maybe you can.
I havn't tested in real-server which has python 3.x yet,
but in my local PC which has Python 3.x I had tested python script.
If you want to do so, you can change interpreter path
(ex. /usr/bin/py3).</li>
  <li>If you got some Exception even if settings are all valid, try to
change invocation scheme. Some of those errors will occur in the reason
of security issues (typically with WAF (WEB Application FireWall)).</li>
  </ol>
  </td>
  </tr>
  </table>
EOT;
	echo '</div>';  // END OF service
	// ###################
	echo '<div id="theme_tab">';
	echo <<<EOT
  <table>
  <tr>
  <td style="vertical-align: top;">
EOT;
	echo '<table class="form-table">';
	// ###########
	$theme_opt_val = WPPYGSH_Opts::instance()->theme;
	$input = "<select id='$theme_field' name='$theme_field'>";
	foreach ( $theme_options as $c ) {
		$sel = $c == $theme_opt_val ? 'selected' : '';
		$input .= "<option value='$c' $sel>$c</option>";
	}
	$input .= '</select>';

	$myurldir = wppygsh_plugins_url( '.' );
	$mythemedir = wppygsh_plugins_url( 'css/theme' );
	$ifrs = "$myurldir/wppygsh-theme-preview.php?theme=$mythemedir/$theme_opt_val.css";
	$ph = "";
	for ( $i = 1; $i <= 50; $i++ ) {
		$ph .= "&nbsp;&nbsp;&nbsp;&nbsp;";
	}
	$desc =<<<EOT
  $ph
  <iframe id="theme_preview_iframe"
    src="$ifrs" width="100%" height="500px"></iframe>
EOT;
	table_row( "Theme", array( $desc ), $input );
	// ###########
	echo '</table>';
	$uploaddir = WPPYGSH_PLUGIN_DIR . 'css/theme/';
	// for security reason, we took relative path rather than fullpath.
	$uploaddir = substr( $uploaddir, strpos( $uploaddir, "wp-content/" ) );
	echo <<<EOT
  </td>
  <td width="320px" style="vertical-align: top;">
  <!-- additional informations area -->
  <h3>Tips</h3>
  <ol>
  <li>You can add style by uploading css to
<span style="text-decoration: underline;">
$uploaddir</span>, and this plugin will detect it automatically.</li>
  </ol>
  </td>
  </tr>
  </table>
EOT;
	echo '</div>';  // END OF theme
	// ###################
	echo '<div id="defaults_tab">';
	echo <<<EOT
  <table>
  <tr>
  <td style="vertical-align: top;">
EOT;
	echo '<table class="form-table">';
	// ###########
	$default_lang_opt_val = WPPYGSH_Opts::instance()->default_lang;
	$input
		= "<input name='$default_lang_field'" .
			" type='text' width='50' value='$default_lang_opt_val'/>";
	table_row(
		"Default language",
		array(
			"Default language used when without explicity \"lang\" ",
			"attribute. For example, `python', `pycon', `console', ",
			"`ruby', `html', `html+php', `java', `javascript', ",
			"`php', ..." ),
		$input );
	// ###########
	echo '</table>';
	echo <<<EOT
  </td>
  <td width="320px" style="vertical-align: top;">
  <!-- additional informations area -->
  <h3>Tips</h3>
  <ol>
  <li>All of available supported languages are described in
$pygments_lexers_href as <strong>Short names</strong>,
for example `pycon' for PythonConsoleLexer.</li>
  </ol>
  </td>
  </tr>
  </table>
EOT;
	echo '</div>';  // END OF defaults
	// ###################
	echo '<div id="advanced_tab">';
	echo <<<EOT
  <table>
  <tr>
  <td style="vertical-align: top;">
EOT;
	echo '<table class="form-table">';
	// ###########
	// load the existing alter shortcodes list
	// from the wordpress options table
	$alter_shortcodes = trim(
		implode( "\n", wppygsh_get_altershortcodes() ), "\n" );
	$input
		= "<textarea name='alter_shortcodes' " .
			"style='width: 200px; height: 100px'>" .
			"$alter_shortcodes</textarea>";
	table_row(
		"Shortcode names (one per line)",
		array(
			"Shortcode name(s) which `WP Pygments Syntax Highlighter' ",
			"should process highlighting. If all are empty, this ",
			"plugin took `pygmentize' as default." ),
		$input );
	// ###########
	echo '</table>';
	echo <<<EOT
  </td>
  <td width="320px" style="vertical-align: top;">
  <!-- additional informations area -->
  <h3>Tips</h3>
  <ol>
  <li>If you want to change shortcode name in order to for example
avoid crash between plugins, you can add or change name of shortcode.</li>
  </ol>
  </td>
  </tr>
  </table>
EOT;
	echo '</div>';  // END OF advanced
	// ###################
	foot();
	// ############################
?>
<?php } ?>
