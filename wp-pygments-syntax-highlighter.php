<?php
/*
Plugin Name: WP Pygments Syntax Highlighter
Plugin URI: https://bitbucket.org/hhsprings/wp-pygments-syntax-highlighter
Description: Syntax highlighting with Pygments. Usage: [pygmentize lang="pycon"]code here[/pygmentize]
Version: 0.2
Author: hhsprings
Author URI: https://bitbucket.org/hhsprings/
*/
/*
 * Copyright (c) 2015, hhsprings <https://bitbucket.org/hhsprings/>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * 
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in
 *   the documentation and/or other materials provided with the
 *   distribution.
 * 
 * - Neither the name of the hhsprings nor the names of its contributors
 *   may be used to endorse or promote products derived from this software
 *   without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
// ----------------------------------------------------------
if ( !defined( 'ABSPATH' ) ) return;

global $wppygsh_plugin_name;
$wppygsh_plugin_name = 'WP Pygments Syntax Highlighter';
if ( !defined( 'WPPYGSH_PLUGIN_NAME' ) )
	define( 'WPPYGSH_PLUGIN_NAME', $wppygsh_plugin_name );

if ( !defined( 'WPPYGSH_VERSION' ) )
	// Don't forget to sync to the version written in preamble.
	define( 'WPPYGSH_VERSION', '0.2' );

if ( !defined( 'WPPYGSH_PLUGIN_DIR' ) ) {
	define( 'WPPYGSH_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
} elseif ( WPPYGSH_PLUGIN_DIR != plugin_dir_path( __FILE__ ) ) {
	add_action(
		'admin_notices',
		create_function( '', 'echo "' . "<div class='error'>" . sprintf(
			"%s detected a conflict; " .
			"please deactivate the plugin located in %s.",
			$wppygsh_plugin_name, WPPYGSH_PLUGIN_DIR) . "</div>" . '";') );
	return;
}
// ----------------------------------------------------------
require_once( 'wppygsh-config.php' );
require_once( 'wppygsh-utils.php' );
require_once( 'wppygsh-logging.php' );
require_once( 'wppygsh-options.php' );
require_once( 'wppygsh-cache-mng.php' );
require_once( 'wppygsh-pygmentize-cgi-glue.php' );
require_once( 'wppygsh-shortcode.php' );
require_once( 'wppygsh-admin-menu.php' );
require_once( 'wppygsh-quicktags.php' );
