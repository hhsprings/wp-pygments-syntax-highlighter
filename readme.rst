WP Pygments Syntax Highlighter
==============================
* Contributors: hhsprings
* Tags: syntax, highlighter, pygments, highlighting
* Requires at least: 4.1
* Tested up to: 4.1
* Stable tag: 0.1
* License: BSD
* License URI: `<http://opensource.org/licenses/bsd-3-clause>`_

A WordPress plugin for syntax highlighting using `pygmentize.cgi <https://bitbucket.org/hhsprings/pygmentize_cgi>`_.

Description
###########

A WordPress plugin for syntax highlighting using `pygmentize.cgi <https://bitbucket.org/hhsprings/pygmentize_cgi>`_.

Installation
############

1. Upload plugin's content to `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Please visit `Settings -> WP Pygments Syntax Highlighter`, and
   complete `External Service Settings`. (You might have to upload
   `Pygments <http://pygments.org/>`_
   package into `/wp-content/plugins/wp-pygments-syntax-highlighter/cgi-bin`.)

Frequently Asked Questions
##########################

Must I have Python installed in my server?
******************************************

* Yes, if `Direct` mode. I've tested with Python 2.7, and, maybe Python 3.x is OK.
* No, if `CGI` mode and set its url to external site.

Must I have Pygments installed in my server?
********************************************

* Yes, if `Direct` mode.
* No, if `CGI` mode and set its url to external site.

Where is official hosting server of pygmentize.cgi?
***************************************************
Now, `<http://hhsprings.pinoko.jp/pygmentize_cgi/cgi-bin/pygmentize.cgi>`_ is only official,
but the server hosting it is not high-end spec,
so, please setup pygmentize.cgi in your own site.

Screenshots
###########
`<wppygsh_screenshot_1.gif>`_

`<wppygsh_screenshot_2.gif>`_

Changelog
#########

0.2
***
Update pygmentize_cgi version from 0.1 to 0.2.

0.1
***
Initial version.
