<?php
/*
 * Copyright (c) 2015, hhsprings <https://bitbucket.org/hhsprings/>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * 
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in
 *   the documentation and/or other materials provided with the
 *   distribution.
 * 
 * - Neither the name of the hhsprings nor the names of its contributors
 *   may be used to endorse or promote products derived from this software
 *   without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

// ----------------------------------------------------------
if ( !defined( 'ABSPATH' ) ) return;
require_once( 'wppygsh-config.php' );


// ----------------------------------------------------------
function wppygsh_entities_to_unicode( $str, $flags ) {
	$str = html_entity_decode( $str, $flags, 'UTF-8' );
	$str = preg_replace_callback(
		"/(&#[0-9]+;)/", function( $m ) {
			return mb_convert_encoding( $m[1], "UTF-8", "HTML-ENTITIES" );
		}, $str);
	return $str;
}
// ----------------------------------------------------------
function wppygsh_html_entity_decode_full( $content ) {
	// mainly, the purpose of html_entity_decode is to
	// become friendly with visual editor.

	// regardless of php behaviour, if entry writer put
	// html_entity_decode attribute to yes, we replace all named entities.
	$flags = ENT_QUOTES | ENT_COMPAT | ENT_HTML401;
	return wppygsh_entities_to_unicode(
		preg_replace(
			array( '/&nbsp;/', '/&apos;/' ),
			array( ' ', "'" ),
			$content), $flags);
}
// ----------------------------------------------------------
function wppygsh_plugins_url( $relpath ) {
	return plugins_url( $relpath, __FILE__ );
}
// ----------------------------------------------------------
