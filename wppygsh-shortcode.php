<?php
/*
 * Copyright (c) 2015, hhsprings <https://bitbucket.org/hhsprings/>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * 
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in
 *   the documentation and/or other materials provided with the
 *   distribution.
 * 
 * - Neither the name of the hhsprings nor the names of its contributors
 *   may be used to endorse or promote products derived from this software
 *   without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

// ----------------------------------------------------------
if ( !defined( 'ABSPATH' ) ) return;

require_once( 'wppygsh-config.php' );
require_once( 'wppygsh-utils.php' );
require_once( 'wppygsh-logging.php' );
require_once( 'wppygsh-options.php' );
require_once( 'wppygsh-cache-mng.php' );
require_once( 'wppygsh-pygmentize-cgi-glue.php' );

// ----------------------------------------------------------
/*
 * process our shortcode.
 */
function pygmentize_code( $attributes, $content='' ) {
	WPPYGSH_Perf_Log::start( "pygmentize_code" );

	global $wpdb;
	global $post;

	$defaults = array(
		'lang' => WPPYGSH_Opts::instance()->default_lang,
		'language' => "",
		'inline_theme_style' => "",
		'hl_lines' => "",
		'linenostart' => 1,
		'linenostep' => 0,
		'linenospecial' => 0,
		'lineanchors' => "",
		'linespans' => "",
		'anchorlinenos' => "",
		'title' => null,
		'alter_ent038' => "",
		'html_entity_decode' => ""  // true, false
	);
	extract( shortcode_atts( $defaults, $attributes ) );
	if ( !empty( $language ) ) {
		$lang = $language;
	}
	$style = "";
	$noclasses = "";
	if ( !empty( $inline_theme_style ) ) {
		$style = $inline_theme_style;
		$noclasses = TRUE;
	}

	if ( !empty( $html_entity_decode ) and $html_entity_decode != "false" ) {
		$content = wppygsh_html_entity_decode_full( $content );
	}
	// WordPress ALWAYS replaces the isolated ampersand to
	// <code>&#038;</code>, so if user really wrote <code>&#038;</code>,
	// we can't distinguish those, and we have no way to disable this
	// behaviour. So, when we got <code>&#038;</code> we can only
	// replace back to <code>&</code>. In order to display
	// <code>&#038;</code> itself, the contents writer can write like this:
	// <pre>
	// [pygmentize alter_ent038="__ENT038__"]
	// %: &#037;, &#37;
	// &: __ENT038__, &#38;
	// [/pygmentize]
	// </pre>
	$content = str_replace( '&#038;', '&', $content );
	if ( !empty( $alter_ent038 ) ) {
		$content = str_replace( $alter_ent038, '&#038;', $content );
	}

	/* ------------------------------ */
	$service_params = array(
		"command" => "format",
		"params" => array(
			'code' => $content,
			'lang' => $lang,
			'style' => $style,
			'noclasses' => $noclasses,

			'hl_lines' => $hl_lines,

			// this plugin use always linenos="inline" because
			// we can control to display or not with using
			// jQuery (wppygsh-tools.js).
			'linenos' => 'inline',

			'linenostart' => (int)$linenostart,
			'linenostep' => (int)$linenostep,
			'linenospecial' => (int)$linenospecial,

			'lineanchors' => $lineanchors,
			'linespans' => $linespans,
			'anchorlinenos' => $anchorlinenos
		)
	);
	$pygcache = new Pygmentized_Cache( $service_params );
	$cache = $pygcache->get_cache();
	if ( empty( $cache ) ) {
		$error = FALSE;
		$output = call_pygmentize_cgi( $service_params, "html", $error );
		if ( !$error ) {
			$cache = $output;
			$pygcache->update_cache( $output );
		}
	}
	/* ------------------------------ */

	$hd = "";
	if ( trim( $title ) != "" ) {
		$hd = '<div class="wppygsh_caption">' . $title . '</div>';
	}
	WPPYGSH_Perf_Log::stop( "" );
	return $hd . '<div class="pygments">' . $cache . '</div>';
}

/*
 * for no_texturize_shortcodes.
 */
function pygmentize_exclude_code( $excluded_shortcodes ) {
	foreach ( wppygsh_get_altershortcodes() as $sc ) {
		$excluded_shortcodes[] = $sc;
	}

	/*
	 * change priority of some filters like
	 * <a href="https://codex.wordpress.org/Function_Reference/wptexturize">wptexturize</a>
	 * so that it runs after shortcodes.
	 */
	foreach ( array( 'wpautop', 'wptexturize') as $f ) {

		if ( has_filter('the_content', $f ) ) {
			remove_filter( 'the_content', $f );
			/* re-add as lower priority than default(10) */
			add_filter( 'the_content', $f, 15 );
		}
	}

	return $excluded_shortcodes;
}

/*
 * add or modify shortcodes
 */
function wppygsh_maint_shortcodes( $orig=null ) {
	if ( !is_null( $orig ) ) {
		foreach ( $orig as $origsc ) {
			remove_shortcode( $origsc );
		}
	}
	foreach ( wppygsh_get_altershortcodes() as $sc ) {
		add_shortcode( $sc, 'pygmentize_code' );
	}
	add_filter( 'no_texturize_shortcodes', 'pygmentize_exclude_code' );
}
wppygsh_maint_shortcodes();

/*
 * register, enqueue css styles and javascripts.
 */
function add_wppygsh_statics() {
	define( 'WPPYGSH_BASESTYLE_NAME', 'pygment-basestyle' );
	define( 'WPPYGSH_STYLE_NAME', 'pygment-style' );

	wp_register_style(
		WPPYGSH_BASESTYLE_NAME,
		wppygsh_plugins_url( 'css/' . 'wppygsh_base.css' ) );
	wp_enqueue_style( WPPYGSH_BASESTYLE_NAME );

	wp_register_style(
		WPPYGSH_STYLE_NAME,
		wppygsh_plugins_url(
			'css/theme/' . WPPYGSH_Opts::instance()->theme . '.css' ) );
	wp_enqueue_style( WPPYGSH_STYLE_NAME );

	wp_enqueue_script(
		'wppygsh-tools',
		wppygsh_plugins_url( 'js/' . 'wppygsh-tools.js' ),
		array( 'jquery' ) );
}
add_action( 'wp_enqueue_scripts', 'add_wppygsh_statics', /*priority=*/15 );
