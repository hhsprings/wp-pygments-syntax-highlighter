<?php
/*
 * Copyright (c) 2015, hhsprings <https://bitbucket.org/hhsprings/>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * 
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in
 *   the documentation and/or other materials provided with the
 *   distribution.
 * 
 * - Neither the name of the hhsprings nor the names of its contributors
 *   may be used to endorse or promote products derived from this software
 *   without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

// ----------------------------------------------------------
if ( !defined( 'ABSPATH' ) ) return;
require_once( 'wppygsh-config.php' );


// ----------------------------------------------------------
final class WPPYGSH_Opts {
	private $opts = array();
	private $intn_cgi_cfg = array();

	private static $inst;

	/*
	 *
	 */
	public static function instance() {
		if ( static::$inst === null ) {
			static::$inst = new static();
		}
		return static::$inst;
	}

	/*
	 *
	 */
	private function __construct() {
		$o = get_option( 'wppygsh_options' );
		if ( !empty( $o ) ) {
			$this->opts = json_decode( $o, true );
		}
		if ( empty( $this->opts['theme'] ) ) {
			$this->opts['theme'] = 'default';
		}
		if ( empty( $this->opts['default_lang'] ) ) {
			$this->opts['default_lang'] = 'pycon';
		}
		if ( empty( $this->opts['call_pygmentize_scheme'] ) ) {
			$this->opts['call_pygmentize_scheme'] = 'direct';
		}
		if ( empty( $this->opts['cgi_url'] ) ) {
			$this->opts['cgi_url'] = 'cgi-bin/pygmentize.cgi';
		}
		if ( empty( $this->opts['alter_shortcodes'] ) ) {
			$this->opts['alter_shortcodes'] = array( 'pygmentize' );
		}
		$internal_cgi_cfgfile = WPPYGSH_PLUGIN_DIR .
							  '/' . 'cgi-bin/pygmentize.cgi.cfg';
		$internal_cgi_cfgcnt = file_get_contents( $internal_cgi_cfgfile );
		$this->intn_cgi_cfg['direct_python_interpreter_path'] = '';
		$this->intn_cgi_cfg['direct_extra_pythonpath'] = "";
		foreach ( explode( "\n", $internal_cgi_cfgcnt ) as $line ) {
			if ( preg_match( "/^python= *(.*) *$/", $line, $matches ) === 1 ) {
				$this->intn_cgi_cfg['direct_python_interpreter_path'] = $matches[1];
			} else if ( preg_match("/^PYTHONPATH=(.*)$/", $line, $matches) === 1 ) {
				$this->intn_cgi_cfg['direct_extra_pythonpath'] = implode(
					"\n", explode( ":", $matches[1] ) );
			}
		}
	}

	/*
	 *
	 */
	public function &__get( $member ) {
		if ( isset( $this->opts[$member] ) ) {
			return $this->opts[$member];
		} elseif ( isset( $this->intn_cgi_cfg[$member] ) ) {
			return $this->intn_cgi_cfg[$member];
		}
	}

	/*
	 *
	 */
	public function __set( $member, $value ) {
		if ( isset( $this->opts[$member] ) ) {
			$this->opts[$member] = $value;
		} elseif ( isset( $this->intn_cgi_cfg[$member] ) ) {
			$this->intn_cgi_cfg[$member] = $value;
		}
	}

	/*
	 *
	 */
	public function update() {
		update_option( 'wppygsh_options', json_encode( $this->opts ) );
		$internal_cgi_cfgfile = WPPYGSH_PLUGIN_DIR .
							  '/' . 'cgi-bin/pygmentize.cgi.cfg';
		$internal_cgi_cfgcnt = "python=" .
							 $this->intn_cgi_cfg['direct_python_interpreter_path'];
		$internal_cgi_cfgcnt .= "\n";
		$internal_cgi_cfgcnt .= "PYTHONPATH=" .
							 implode(":",
									 explode("\n",
											 trim(
												 $this->intn_cgi_cfg[
													 'direct_extra_pythonpath'] ) ) );
		file_put_contents( $internal_cgi_cfgfile, $internal_cgi_cfgcnt );
	}

	/*
	 *
	 */
	public function delete() {
		delete_option( 'wppygsh_options' );
	}
}
// ----------------------------------------------------------
/*
* get the alternative shortcodes from the wordpress options table
*/
function wppygsh_get_altershortcodes() {
	$result = WPPYGSH_Opts::instance()->alter_shortcodes;

	// if no values have been set, revert to the defaults
	if ( !$result ) {
		$result[] = "pygmentize";
	}
	return $result;
}

/*
* get the pygmentize.cgi as shell command from the wordpress options table
*/
function wppygsh_get_service_shellcmd( &$wd ) {
	$script = 'cgi-bin/pygmentize.cgi';
	// NOTE:
	// Because we need to keep the python script portable, the script has
	// no magic-number (a.k.a shellbang, like #! /usr/bin/python or
	// #! /bin/env python).

	if ( !preg_match( "/^\//", $script ) ) {
		$script = WPPYGSH_PLUGIN_DIR . '/' . $script;
	}
	$script = escapeshellarg( $script );
	$wd = dirname( $script );
	return $script;
}

/*
* get the pygmentize.cgi as cgi from the wordpress options table
*/
function wppygsh_get_service_cgi() {
	$result = str_replace( "\\/", "/", WPPYGSH_Opts::instance()->cgi_url );
	if ( preg_match( "/^\/\//", $result ) or
		 preg_match( "/:/", $result ) ) {
		return $result;
	}
	return wppygsh_plugins_url( $result );
}
