# -*- coding: utf-8 -*-
#
# Copyright (c) 2015, hhsprings <https://bitbucket.org/hhsprings>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 
# - Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
# 
# - Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in
#   the documentation and/or other materials provided with the
#   distribution.
# 
# - Neither the name of the hhsprings nor the names of its contributors
#   may be used to endorse or promote products derived from this software
#   without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# ==================================================================
#
# Module Setup Section to get proper behaviour as CGI
#
# ==================================================================
__version__ = '0.2'
import sys
import os
import re
ap_all = []
for line in open("pygmentize.cgi.cfg", "rb").readlines():
    m = re.match(r"^PYTHONPATH *= *([^\s]+)$", line.rstrip())
    if m:
        for ap in m.group(1).split(":"):
            ap_all.append(os.path.abspath(ap))
        break
ap_all.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), "."))
sys.path = ap_all + sys.path

import json
if sys.platform == "win32":
    import os, msvcrt
    msvcrt.setmode(sys.stdout.fileno(), os.O_BINARY)
    msvcrt.setmode(sys.stderr.fileno(), os.O_BINARY)


import logging
from logging.handlers import RotatingFileHandler
loggerhandler = RotatingFileHandler(
    os.path.abspath(__file__) + ".log",
    maxBytes=1*1024*1024,
    backupCount=3,
    encoding="utf-8")
loggerfomatter = logging.Formatter(
    '[%(asctime)s %(levelname)-8s] %(message)s'
    )
loggerhandler.setFormatter(loggerfomatter)
logger = logging.getLogger(__name__)
logger.addHandler(loggerhandler)
logger.setLevel(logging.INFO)


try:
    import StringIO
    StringIO = StringIO.StringIO
    BytesIO = StringIO
    import codecs
    import urllib
    def unquote(s):
        return urllib.unquote(s)
    def quote(s):
        return urllib.quote(s)
    _PY27 = True
except ImportError:
    # python 3
    import io
    from io import StringIO
    from io import BytesIO
    if sys.stdin.encoding != 'utf-8':
        # Windows?
        sys.stdin = io.TextIOWrapper(sys.stdin.buffer, encoding="utf-8")
    import urllib.parse  # Python 3.x
    def unquote(s):
        return urllib.parse.unquote(s)
    def quote(s):
        return urllib.parse.quote(s)
    _PY27 = False


# ----------------------------------------------------------------------
def _client_accepts_sorted(reqhdr_key):
    aster_dict = {
        "HTTP_ACCEPT_CHARSET": {"*": "utf-8"},
        "HTTP_ACCEPT_ENCODING": {"*": "gzip"},
        "HTTP_ACCEPT": {"*/*": "text/html"},
        }[reqhdr_key]

    def _sorted(reqhdr):
        if not reqhdr:
            return []

        _ = [
            re.sub(r"\s+", "",
                   (ae if ";q=" in ae else "%s;q=1" % ae)).split(";q=")
            for ae in reqhdr.split(",")]
    
        return sorted(
            [(float(ac[1]), ac[0]) for ac in _], key=lambda k: 1.0 - k[0])

    res = _sorted(os.environ.get(reqhdr_key))
    if reqhdr_key == "HTTP_ACCEPT_CHARSET":
        if not res:
            res.append((1.0, "utf-8"))
        elif not filter(lambda v: v[1] == "iso-8859-1", res):
            for i, (q, ac) in enumerate(res):
                if q < 1:
                    res.insert(i, (1.0, "iso-8859-1"))
                    break
    elif reqhdr_key == "HTTP_ACCEPT_ENCODING":
        if not res:
            res.append((1.0, "identity"))
    elif reqhdr_key == "HTTP_ACCEPT":
        if not res:
            res.append((1.0, "text/html"))
    for q, ac in res:
        if q > 0:
            if ac in aster_dict:
                yield aster_dict[ac]
            else:
                yield ac


# ----------------------------------------------------------------------
class _Responder(object):
    def __init__(self):
        self._st_map = {
            400: (
                "400", "Bad Request",
                "Your request contains malformed parameters and cannot" + \
                    " be fullfilled."),
            406: (
                "406", "Not Acceptable",
                "No appropriate representation of the requested resource" + \
                    " could not be found on this server."),
            501: (
                "501", "Not Implemented",
                "GET to this resource not supported."),
            }
        self.status = ("200", "OK", "")

        # Content-Type
        supported_content_type = {
            "text/html": "html",
            "text/plain": "html",
            "application/json": "json",
            "text/javascript": "json",
            }
        self.content_type = ("text/html", supported_content_type["text/html"])
        for ct in _client_accepts_sorted("HTTP_ACCEPT"):
            if ct in supported_content_type:
                self.content_type = (ct, supported_content_type[ct])
                break
        else:
            self.status = self._st_map[406]

        # charset
        self.content_charset = ("iso-8859-1", "iso-8859-1")
        if self.status[0] != "406":
            if self.content_type[1] == "json":
                # if json, ignore charset. (force utf-8.)
                self.content_charset = ("utf-8", "utf-8")
            else:
                import iana_character_sets
                for ac in _client_accepts_sorted("HTTP_ACCEPT_CHARSET"):
                    try:
                        self.content_charset = (
                            iana_character_sets.lookup(ac)[0], ac)
                        break
                    except LookupError:
                        pass
                else:
                    self.status = self._st_map[406]

        # Content-Encoding
        def _gzip(content):
            import gzip
            out = BytesIO()
            f = gzip.GzipFile(fileobj=out, mode='w')
            f.write(content.encode(self.content_charset[0]))
            f.close()
            return out.getvalue()

        def _deflate(content):
            def _ord(s):
                if isinstance(s, (int,)):  # Python 3.x
                    return s
                return ord(s)

            import zlib
            cmprs = zlib.compressobj()
            deflated = cmprs.compress(
                content.encode(self.content_charset[0]))
            deflated += cmprs.flush()
            return deflated

        supported = {
            "gzip": _gzip,
            "deflate": _deflate,
            "identity": lambda content: content,
            }
        use_ae = None
        for ae in _client_accepts_sorted("HTTP_ACCEPT_ENCODING"):
            if ae in supported:
                use_ae = ae
                break
        if not use_ae:
            use_ae = "identity"
            self.status = self._st_map[406]

        def content_encode(content):
            c = supported[use_ae](content)
            return use_ae, len(c), c

        self.content_encode = content_encode

        # detect stream writer
        if _PY27:
            bstdout = sys.stdout
            sstdout = codecs.getwriter(
                self.content_charset[0])(sys.stdout)
        else:
            # python 3
            sstdout = io.TextIOWrapper(
                sys.stdout.buffer, encoding=self.content_charset[0])
            bstdout = sstdout.buffer
        if use_ae == "identity":
            bstdout = sstdout
        self.body_stream = bstdout

    def change_status(self, status):
        self.status = self._st_map[status];

    def response(self, content=""):
        if self.status[0] != "200":
            if self.content_type[0] == "html":
                content = "<p>" + self.status[2] + "</p>"
            else:
                content = self.status[2]

        content_encoding, content_length, content = \
            self.content_encode(content)

        content_type = self.content_type[0]
        if self.content_type[1] != "json":
            content_type += "; charset=" + self.content_charset[1]
        sys.stdout.write("Status: " + " ".join(self.status[:-1]) + "\n")
        sys.stdout.write("Content-Type: %s\n" % content_type)
        if content_encoding != "identity":
            sys.stdout.write("Content-Length: %d\n" % content_length)
            sys.stdout.write("Content-Encoding: %s\n" % content_encoding)

        sys.stdout.write("\n")
        sys.stdout.flush()

        self.body_stream.write(content)
        self.body_stream.flush()

        exst = 0 if self.status[0] == "200" else -int(self.status[0][0])
        sys.exit(exst)

        
# ==================================================================
#
# Pygmentize Functionality Section
#
# ==================================================================
def _adjust_params(language, code):
    
    if language == "php":
        if b"<?php" not in code:
            return dict(startinline=True), code
    return {}, code


def _parse_hl_lines(fromform):
    """
    >>> _parse_hl_lines("")
    []
    >>> _parse_hl_lines("32 36-38")
    [32, 36, 37, 38]
    >>> _parse_hl_lines("32  36 - 38")
    [32, 36, 37, 38]
    >>> _parse_hl_lines("range(5) range(10, 15) range(20, 30, 2) 32 36-38")
    [0, 1, 2, 3, 4, 10, 11, 12, 13, 14, 20, 22, 24, 26, 28, 32, 36, 37, 38]
    >>> _parse_hl_lines("range(5), range(10, 15), range(20, 30, 2), 32, 36-38")
    [0, 1, 2, 3, 4, 10, 11, 12, 13, 14, 20, 22, 24, 26, 28, 32, 36, 37, 38]
    """
    not_ranges = re.sub(r'\s+', ' ', re.sub(r"\s+-\s+", "-", fromform))
    ranges = re.findall(r"\b(range\([^()]+\))", not_ranges)
    if ranges:
        not_ranges = re.sub(
            "(%s)" % re.sub(r"([()])", r"\\\1", "|".join(ranges)),
            "", fromform)
        ranges = [re.sub(r"\brange\(([^()]+)\)", r"\1", r) for r in ranges]
    result = []
    for s in re.split(r"[\s,]+", not_ranges):
        if not s.strip():
            continue
        if "-" in s:
            spl = s.split("-")
            result.extend(range(int(spl[0]), int(spl[1]) + 1))
        else:
            result.append(int(s))
    for sl in ranges:
        result.extend(range(*map(int, sl.split(","))))
    result = list(set(result))
    result.sort()
    return result
# ----------------------------------------------------------------------


# ----------------------------------------------------------------------
def _format(form, outtype, content_charset):
    import pygments
    from pygments.lexers import get_lexer_by_name
    from pygments.formatters.html import HtmlFormatter

    lang = form.get("lang", "pycon")
    code = form["code"].encode("utf-8")
    def _my_parse_boolean(v):
        def _lower(s):
            if hasattr(s, "lower"):
                return s.lower()
            return s
        return _lower(v) in (True, "true", 1, "1")

    # NOTE: though formatters.html.HtmlFormatter can take encoding attribute,
    # but we don't use it.
    _formatter_params = dict(
        nowrap=form.get("nowrap"),
        full=form.get("full"),
        title=form.get("title"),
        # --
        # combination between style and noclasses will give you
        # ability to dynamically and locally changing theme without
        # special implement, but note that it will produce large html.
        style=form.get("style", ""),
        noclasses=_my_parse_boolean(form.get("noclasses")),
        classprefix=form.get("classprefix", ""),  # avoid crash css classes
        cssclass=form.get("cssclass", ""),  # default: ``'highlight'``
        cssstyles=form.get("cssstyles", ""),
        prestyles=form.get("prestyles", ""),
        #cssfile=form.get("cssfile", ""),
        #noclobber_cssfile=_my_parse_boolean(form.get("noclobber_cssfile")),
        linenos=form.get("linenos", ""),
        hl_lines = _parse_hl_lines(form.get("hl_lines", "")),
        linenostart=int(form.get("linenostart", 1)),
        linenostep=int(form.get("linenostep", 0)),
        linenospecial=int(form.get("linenospecial", 0)),
        nobackground=_my_parse_boolean(form.get("nobackground")),
        lineseparator=form.get("lineseparator"),
        lineanchors=form.get("lineanchors", ""),  # <a name="foo-nn"></a>
        linespans=form.get("linespans", ""),  # <span id="foo-nn"></span>
        anchorlinenos=_my_parse_boolean(form.get("anchorlinenos")),
        #tagsfile=form.get("tagsfile"),
        #tagurlformat=form.get("tagurlformat"),
        )
    formatter_params = dict((
            (k, _formatter_params[k])
            for k in filter(
                lambda k: _formatter_params[k], _formatter_params)))

    extra_params, code = _adjust_params(lang, code)
    lexer = get_lexer_by_name(lang, **extra_params)
    formatter = HtmlFormatter(**formatter_params)

    render_result_stream = StringIO()
    pygments.highlight(code, lexer, formatter, render_result_stream)
    render_result = render_result_stream.getvalue()
    if _my_parse_boolean(form.get("full")):
        render_result = re.sub(
            r'(\scontent="text/html; charset=)[^<>"]+(">)',
            r'\1%s\2' % content_charset, render_result)
    if outtype == "html":
        return render_result
    else:
        return json.dumps(render_result)
# ----------------------------------------------------------------------


# ----------------------------------------------------------------------
def _lexers(form, outtype):
    import pygments
    from pygments.lexers import get_all_lexers

    # ["name", "aliases", "filenames", "mimetypes"]
    fields = form.get(
        "fields", ["name", "aliases"])
    result = []
    for name, aliases, filenames, mimetypes in get_all_lexers():
        row = []
        if "name" in fields:
            row.append(name)
        if "aliases" in fields:
            row.append(aliases)
        if "filenames" in fields:
            row.append(filenames)
        if "mimetypes" in fields:
            row.append(mimetypes)
        result.append(row)
    result.sort()
    if outtype == "html":
        res = ["<table class='pygments_lexers_table'><tr><th>" + "</th><th>".join(fields) + "</th></tr>"]
        for row in result:
            res.append("<tr>")
            for i in range(len(fields)):
                res.append("<td>")
                if row[i]:
                    if isinstance(row[i], (list, tuple,)):
                        res.append("<ul><li>")
                        res.append("</li><li>".join(row[i]))
                        res.append("</li></ul>")
                    else:
                        res.append(row[i])
                res.append("</td>")
            res.append("</tr>")
        res.append("</table>")
        return "".join(res)

    return json.dumps(result)
# ----------------------------------------------------------------------


# ==================================================================
#
# Main Section as CGI
#
# ==================================================================
def _parse_not_json(postdata):
    def _mergedicts(d1, d2):
        for k in set(d1.keys()).union(d2.keys()):
            if k in d1 and k in d2:
                if isinstance(d1[k], (dict,)) and \
                        isinstance(d2[k], (dict,)):
                    yield (k, dict(_mergedicts(d1[k], d2[k])))
                elif isinstance(d1[k], (list, tuple,)) and \
                        isinstance(d2[k], (list, tuple,)):
                    yield (k, d1[k] + d2[k])
                else:
                    yield (k, d2[k])
            elif k in d1:
                yield (k, d1[k])
            else:
                yield (k, d2[k])

    def _parse_key(s):
        toplevel = re.search(r"^([^[]*)", s).group(1)
        subs = re.findall(r"\[[^]]*\]", s)
        subs = [re.sub(r"\[(.*)\]", r"\1", sb) for sb in subs]
        return [toplevel] + subs

    res = {}
    for s in postdata.replace("+", " ").split("&"):
        k, v = s.split("=")
        spl_k = _parse_key(unquote(k))
        v = unquote(v).decode("utf-8")
        tmp = {}
        cur = tmp
        leafkey = None
        leafdict = None
        for i, k in enumerate(spl_k):
            if i < len(spl_k) - 1 and spl_k[i + 1] == '':  # list
                if k not in cur:
                    cur[k] = []
                cur = cur[k]
            elif k:
                leafkey = k
                leafdict = cur
                if k not in cur:
                    cur[k] = {}
                cur = cur[k]
        if isinstance(cur, (list,)):
            cur.append(v)
        else:
            leafdict[leafkey] = v
        res = dict(_mergedicts(res, tmp))
    return res


def main():
    _responder = _Responder()
    
    if _responder.status[0] == "200":
        form = None
        method = os.environ.get('REQUEST_METHOD')
        if method == "GET":
            logger.info("GET")
            contents = open("pygmentize_cgi.html", "r").read().decode("utf-8")
            if _responder.content_type[1] == "json":
                contents = json.dumps(contents)
            elif _responder.content_charset[0] != "utf-8":
                orig_meta = '    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />'
                repl_meta = '    <meta http-equiv="Content-Type" content="text/html; charset=%s" />' % _responder.content_charset[1]
                conts = contents.partition(orig_meta)
                contents = "".join([
                    conts[0], repl_meta, conts[2]
                    ])
                _responder.response(contents)
        elif method != "POST":
            _responder.change_status(501)
            _responder.response()
        else:
            ctype = os.environ.get('CONTENT_TYPE', "")
            clength = -1
            if os.environ.get('CONTENT_LENGTH'):
                clength = int(os.environ.get('CONTENT_LENGTH'))
            postdata = sys.stdin.read(clength)
            try:
                if not ctype or re.search(r"\bapplication/json\b", ctype):
                    form = json.loads(postdata)
                elif re.search(
                    r"\bapplication/x-www-form-urlencoded\b", ctype):
                    try:
                        form = json.loads(unquote(postdata))
                    except ValueError:
                        form = _parse_not_json(postdata)
            except ValueError:
                pass
        if not form or "command" not in form or "params" not in form:
            _responder.change_status(400)
            _responder.response()
        else:
            command = form.get("command")
            if command not in ("format", "lexers"):
                _responder.change_status(400)
                _responder.response()
            elif command == "format":
                _responder.response(
                    _format(
                        form["params"],
                        _responder.content_type[1],
                        _responder.content_charset[1]))
            else:
                _responder.response(
                    _lexers(
                        form["params"],
                        _responder.content_type[1]))
    else:
        _responder.response()
    # ----------------------------------------------------------------------

try:
    #logger.debug(json.dumps(sorted(os.environ.items()), indent=2))
    main()
except Exception:
    logger.exception("UNEXPECTED")
    raise  # cause 500 Server Error
