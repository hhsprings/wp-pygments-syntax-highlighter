<?php
/*
 * Copyright (c) 2015, hhsprings <https://bitbucket.org/hhsprings/>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * 
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in
 *   the documentation and/or other materials provided with the
 *   distribution.
 * 
 * - Neither the name of the hhsprings nor the names of its contributors
 *   may be used to endorse or promote products derived from this software
 *   without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

// ----------------------------------------------------------
if ( !defined( 'ABSPATH' ) ) return;

require_once( 'wppygsh-config.php' );
require_once( 'wppygsh-utils.php' );
require_once( 'wppygsh-logging.php' );
require_once( 'wppygsh-options.php' );
require_once( 'wppygsh-pygmentize-cgi-glue.php' );
require_once( 'wppygsh-ui-common.php' );

// ----------------------------------------------------------
/*
 * for writers functionality
 */
if ( !function_exists( 'wppygsh_add_my_quicktags' ) ) {
    function wppygsh_add_my_quicktags() {
        // Print our Javascript code when this PHP function is called
        $shortcode_name = wppygsh_get_altershortcodes();
        $shortcode_name = $shortcode_name[0];
        $lexers = get_pygments_lexers();
        $default_lang = WPPYGSH_Opts::instance()->default_lang;
?>
    <script type="text/javascript">
    /**
     * Add custom Quicktag buttons to the WordPress editor
     * for ver. 3.3 and above only
     * Params for the addButton function are:
     *  - Button HTML ID (required)
     *  - Opening Tag (required)
     *  - Closing Tag (required)
     *  - Access key, accesskey="" attribute for the button (optional)
     *  - Title, title="" attribute (optional)
     *  - Priority/position on bar, 1-9 = first, 11-19 = second,
     *    21-29 = third, etc. (optional)
     */
    //QTags.addButton( 'php', 'PHP', '[ php gutter="false" ]', '[ /php]' );
    //QTags.addButton( 'js', 'JS', '[ js gutter="false"]', '[ /js]' );
    //QTags.addButton( 'h2', 'H2', '< h2>', '< /h2>' );
    //QTags.addButton( 'h3', 'H3', '< h3>', '< /h3>' );

    /*
     * Add function callback button
     * Params are the same as above except the 'Opening Tag' param
     * becomes the callback function's name
     * and the 'Closing Tag' is ignored.
     */
    QTags.addButton('pygmentize', 'Pygmentize', prompt_user);

    var _wppygsh_generated_tb_submit_callback = function(opentag) {};

    function _wppygsh_show_tb(cb, prompt_user_this, e, c, ed) {
        _wppygsh_generated_tb_submit_callback = function (opentag) {
            cb(opentag);
            // now we've defined all the tagStart, tagEnd and openTags
            // we process it all to the active window
            QTags.TagButton.prototype.callback.call(
                prompt_user_this, e, c, ed);
        }
        var width = jQuery(window).width();
        var H = jQuery(window).height();
        var W = (720 < width) ? 720 : width;
        tb_show(
            'Insert Pygmentize Shortcode',
            '#TB_inline?modal=true&inlineId=wppygsh-form&width=' + W);
    }
    function prompt_user(e, c, ed) {
        var prompt_user_this = this;
        var closeTag = '[/<?php echo $shortcode_name ?>]\n';
        if (ed.canvas.selectionStart !== ed.canvas.selectionEnd) {
            // we have a selection in the editor
            _wppygsh_show_tb(function(opentag) {
                prompt_user_this.tagStart = opentag;
                prompt_user_this.tagEnd = closeTag;
            }, prompt_user_this, e, c, ed);
        } else if (ed.openTags) {
            // we have an open tag
            var ret = false, i = 0;
            while (i < ed.openTags.length) { // find if it's ours?
                ret = ed.openTags[i] == prompt_user_this.id ? i : false;
                i++;
            }
            if (ret === false) {
                // the open tags don't include 'pygmentize'
                _wppygsh_show_tb(function(opentag) {
                    prompt_user_this.tagStart = opentag;
                    prompt_user_this.tagEnd = false;
                    if (!ed.openTags) {
                        ed.openTags = [];
                    }
                    ed.openTags.push(prompt_user_this.id);
                    e.value = '/' + e.value;
                }, prompt_user_this, e, c, ed);
            } else {
                // otherwise close the 'pygmentize' tag
                ed.openTags.splice(ret, 1);
                prompt_user_this.tagStart = closeTag;
                e.value = prompt_user_this.display;
                // now we've defined all the tagStart, tagEnd and
                // openTags we process it all to the active window
                QTags.TagButton.prototype.callback.call(
                    prompt_user_this, e, c, ed);
            }
        } else {
            // last resort, no selection and no open tags
            // so prompt for input and just open the tag
            _wppygsh_show_tb(function(opentag) {
                prompt_user_this.tagStart = opentag;
                prompt_user_this.tagEnd = false;
                if (!ed.openTags) {
                    ed.openTags = [];
                }
                ed.openTags.push(prompt_user_this.id);
                e.value = '/' + e.value;
            }, prompt_user_this, e, c, ed);
        }
    }
    // executes this when the DOM is ready
    jQuery(function($) {
        // creates a form to be displayed everytime the button is clicked
        // you should achieve this using AJAX instead of direct html code
        // like this
        var form = jQuery('<div id="wppygsh-form">\
<!--...-->\
<p class="submit">\
<input type="button" class="button-primary wppygsh-submit" \
value="Insert Shortcode" name="submit" />\
&nbsp;<input type="button" class="button-primary wppygsh-cancel" \
value="<?php _e("Cancel") ?>" name="cancel" />\
</p>\
<!-- class="form-table" -->\
<table id="wppygsh-table">\
<!--...-->\
<tr>\
<th><label for="wppygsh-lang">lang</label></th>\
<td><input type="text" id="wppygsh-lang" name="lang" \
     value="<?php echo $default_lang ?>" style="width: 180px;" />\
&larr;<select id="wppygsh-lang-pulldown"><?php echo $lexers ?></select>\
<span class="wppygsh-description">\
specify the lang, for example ``pycon``.</span></td>\
</tr>\
<!--...-->\
<tr>\
<th><label for="wppygsh-title">title</label></th>\
<td><input type="text" id="wppygsh-title" name="title" \
value="" style="width: 460px;" />\
<span class="wppygsh-description">specify the title.</span></td>\
</tr>\
<!--...-->\
<tr>\
<th><label for="wppygsh-hl_lines">hl_lines</label></th>\
<td><input type="text" id="wppygsh-hl_lines" name="hl_lines" \
value="" style="width: 400px;" />\
<span class="wppygsh-description">\
If you want to highlight specified line(s), set hl_lines. \
For multiple highliting, you can specify as following:\
<ol>\
    <li>Both space and comma are a separator, so, you can set \
it as <code>1 2 4</code> or <code>1,2,4</code></li>\
    <li>Hypen-joined <code>2-5</code> means \
<code>2, 3, 4, 5</code> (including 5).</li>\
    <li>You can use an alterative special form like \
<code>range(1, 2, 2)</code> to ranged highlighting, \
that means highliting lines [start, end, step).</li>\
</ol>\
</span></td>\
</tr>\
<!--...-->\
<tr>\
<th><label for="wppygsh-html_entity_decode">\
html_entity_decode</label></th>\
<td><input type="checkbox" id="wppygsh-html_entity_decode" \
name="html_entity_decode" />\
<span class="wppygsh-description">\
Check if the target code that you want to highlight is \
already html-entity-encoded.</span></td>\
</tr>\
<!--...-->\
<tr>\
<th><label for="wppygsh-linespans">linespans</label></th>\
<td><input type="text" id="wppygsh-linespans" name="linespans" \
value="" style="width: 150px;" />\
<span class="wppygsh-description">\
If set to a nonempty string, e.g. ``foo``, the formatter of \
pygments will wrap each output line in a span tag with an ``id`` \
of ``foo-linenumber`` \
(ex: <code>&lt;span id="foo-52"&gt;...&lt;/span&gt;</code>). \
This allows easy access to lines via javascript.</span></td>\
</tr>\
<!--...-->\
<tr>\
<th><label for="wppygsh-linenostart">linenostart</label></th>\
<td><input type="text" id="wppygsh-linenostart" name="linenostart" \
     onkeypress="return event.charCode >= 48 && event.charCode <= 57" \
     value="" style="width: 50px;" />\
<span class="wppygsh-description">\
The line number for the first line. \
(Notice: Don\'t mix with hl_lines, because pygments can\'t.) </span></td>\
</tr>\
<!--...-->\
<tr>\
<th><label for="wppygsh-linenostep">linenostep</label></th>\
<td><input type="text" id="wppygsh-linenostep" name="linenostep" \
     onkeypress="return event.charCode >= 48 && event.charCode <= 57" \
     value="" style="width: 50px;" />\
<span class="wppygsh-description">\
If set to a number n &gt; 1, only every nth line number is printed.</span></td>\
</tr>\
<!--...-->\
<tr>\
<th><label for="wppygsh-linenospecial">linenospecial</label></th>\
<td><input type="text" id="wppygsh-linenospecial" name="linenospecial" \
     onkeypress="return event.charCode >= 48 && event.charCode <= 57" \
     value="" style="width: 50px;" />\
<span class="wppygsh-description">\
If set to a number n &gt; 0, every nth line number is given \
the CSS class ``"special"``.</span></td>\
</tr>\
<!--...-->\
<tr>\
<th><label for="wppygsh-alter_ent038">alter_ent038</label></th>\
<td><input type="text" id="wppygsh-alter_ent038" name="alter_ent038" \
value="" style="width: 200px;" />\
<span class="wppygsh-description">\
WordPress ALWAYS replaces the isolated ampersand to \
<code>&amp;#038;</code>, \
so if user really wrote <code>&amp;#038;</code>, \
we can\'t distinguish those, \
and we have no way to disable this behaviour. So, when we got \
<code>&amp;#038;</code> \
we can only replace back to <code>&amp;</code>. \
In order to display <code>&amp;#038;</code> itself, the contents \
writer can write like this: <br/>\
<pre>\
[pygmentize alter_ent038="__ENT038__"]<br/>\
%: &amp;#037;, &amp;#37;<br/>\
&amp;: __ENT038__, &amp;#38;<br/>\
[/pygmentize]<br/>\
</pre> \
</span></td>\
</tr>\
<!--...-->\
</table>\
<!--...-->\
<p class="submit">\
<input type="button" class="button-primary wppygsh-submit" \
value="Insert Shortcode" name="submit" />\
&nbsp;<input type="button" class="button-primary wppygsh-cancel" \
value="<?php _e("Cancel") ?>" name="cancel" />\
</p>\
</div>');
        var table = form.find('table');
        table.find("tr").css("vertical-align", "top");
        form.appendTo('body').hide();

        var lang_input = form.find("#wppygsh-lang");
        var lang_pulldown = form.find("#wppygsh-lang-pulldown");
        var lang_pulldown_options = form.find("#wppygsh-lang-pulldown option");
        lang_pulldown.on("change", function () {
            var e = $(this);
            lang_input.val(e.val());
        });
        lang_input.on("change", function () {
            var e = $(this);
            var raw_text = e.val();
            lang_pulldown.val(raw_text);
            if (lang_pulldown.val() === null) {
                lang_pulldown_options.each(function(i) {
                    if ($(this).val().lastIndexOf(raw_text, 0) === 0) {
                        var v = $(this).val();
                        e.val(v);
                        lang_pulldown.val(v);
                        return false;
                    }
                });
            }
        });

        // handles the click event of the submit button
        form.find('.wppygsh-submit').click(function() {
            // defines the options and their default values
            // again, this is not the most elegant way to do this
            // but well, this gets the job done nonetheless
            var options = {
                'title': '',
                'lang': '',
                'hl_lines': '',
                'html_entity_decode': '',
                'linespans': '',
                'linenostart': '',
                'linenostep': '',
                'linenospecial': '',
                'alter_ent038': '',
            };
            var shortcode = '[<?php echo $shortcode_name ?>';

            for (var attr in options) {
                var ctrl = table.find('#wppygsh-' + attr);
                var ctrltype = ctrl.attr('type');
                var value = "";
                if (ctrltype == "text") {
                    value = ctrl.val();
                    if (attr != "lang") {
                        // clear for the next time after tb_remove().
                        ctrl.val("");
                    }
                } else if (ctrltype == "checkbox") {
                    value = ctrl.attr("checked");
                    if (value === undefined) {
                        value = ctrl.prop("checked"); //  jQuery 1.9+
                    }
                    value = (value == "checked" ? "true" : "");
                }

                if (attr == "lang") {
                    // if attr is lang, put always lang=, because default
                    // lang when user don't specify is customizable using
                    // admin page setting.
                    if (value === "") {
                        value = "<?php echo $default_lang ?>";
                    }
                    shortcode += ' ' + attr + '="' + value + '"';
                } else if (value !== options[attr]) {
                    // attaches the attribute to the shortcode only if
                    // it's different from the default value (except lang)
                    shortcode += ' ' + attr + '="' + value + '"';
                }
            }

            shortcode += ']\n';

            // set the shortcode into global callback
            // `_wppygsh_generated_tb_submit_callback'
            _wppygsh_generated_tb_submit_callback(shortcode);

            // closes Thickbox
            tb_remove();
        });
        // handles the click event of the cancel button
        form.find('.wppygsh-cancel').click(function() {
            // closes Thickbox
            tb_remove();
        });
        // add functionality to toggle descriptions.
        var desc = form.find('.wppygsh-description');
        desc.each(function (index) {
            var e = $(this);
            e.html('&nbsp;<span class="wppygsh-description-toggle">' +
                   '&#xff1f;</span>' +
                   '<span class="wppygsh-description-desc" ' +
                   'style="display: none;">' + e.html() + '</span>');
            e.find(".wppygsh-description-toggle").css({
                "text-decoration": "underline",
                "color": "blue",
                'cursor': 'pointer'
                });
            e.find(".wppygsh-description-toggle").on("click", function() {
                var d = e.find(".wppygsh-description-desc");
                if (d.css("display") == "none") {
                    d.css("display", "block");
                } else {
                    d.css("display", "none");
                }
            });
        });
    });
    </script>

<?php
    }
    // We can attach it to 'admin_print_footer_scripts' (for admin-only)
    // or 'wp_footer' (for front-end only)
    add_action(
        'admin_print_footer_scripts', 
        'wppygsh_add_my_quicktags' );
}
?>
